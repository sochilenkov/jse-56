package ru.t1.sochilenkov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.sochilenkov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.sochilenkov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.sochilenkov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.dto.IUserDtoService;
import ru.t1.sochilenkov.tm.dto.model.UserDTO;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.UserNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private IUserDtoRepository getRepository() {
        return context.getBean(IUserDtoRepository.class);
    }

    @NotNull
    private ITaskDtoRepository getTaskRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @NotNull
    private IProjectDtoRepository getProjectRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @SneakyThrows
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user = new UserDTO();
        try {
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull UserDTO user = new UserDTO();
        try {
            if (repository.isLoginExist(login)) throw new ExistsLoginException();
            if (password == null || password.isEmpty()) throw new PasswordEmptyException();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            if (email != null && !email.isEmpty()) {
                if (repository.isEmailExist(email)) throw new ExistsEmailException();
                user.setEmail(email);
            }
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull UserDTO user = new UserDTO();
        try {
            if (repository.isLoginExist(login)) throw new ExistsLoginException();
            if (password == null || password.isEmpty()) throw new PasswordEmptyException();
            if (role == null) throw new RoleEmptyException();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void clear() {
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable UserDTO user) {
        if (user == null) throw new EntityNotFoundException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull List<UserDTO> findAll() {
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull List<UserDTO> result;
        try {
            result = repository.findAll();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityManager entityManagerProject = projectRepository.getEntityManager();
        @NotNull final EntityManager entityManagerTask = taskRepository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findByEmail(email);
            if (user == null) throw new UserNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(user);
            taskRepository.clear(user.getId());
            projectRepository.clear(user.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerProject.close();
            entityManagerTask.close();
        }
    }

    @Override
    public void remove(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityManager entityManagerProject = projectRepository.getEntityManager();
        @NotNull final EntityManager entityManagerTask = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(user);
            @NotNull final String userId = user.getId();
            taskRepository.clear(userId);
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
            entityManagerProject.close();
            entityManagerTask.close();
        }
    }

    @Override
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        boolean result;
        try {
            result = repository.isLoginExist(login);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        boolean result;
        try {
            result = repository.isEmailExist(email);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
