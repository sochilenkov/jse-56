package ru.t1.sochilenkov.tm.repository.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.sochilenkov.tm.api.repository.model.ISessionRepository;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.model.IUserService;
import ru.t1.sochilenkov.tm.configuration.ServerConfiguration;
import ru.t1.sochilenkov.tm.migration.AbstractSchemeTest;
import ru.t1.sochilenkov.tm.model.Session;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.SessionConstant.INIT_COUNT_SESSIONS;

@Category(UnitCategory.class)
public class SessionRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private ISessionRepository repository;

    @NotNull
    private List<Session> sessionList;

    @Nullable
    private List<User> userList;

    @NotNull
    private final IPropertyService propertyService = context.getBean(IPropertyService.class);

    @NotNull
    private final IUserService userService = context.getBean(IUserService.class);

    @Nullable
    private static User USER;

    private static long USER_ID_COUNTER;

    @Nullable
    private static EntityManager ENTITY_MANAGER;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        repository = context.getBean(ISessionRepository.class);
        sessionList = new ArrayList<>();
        userList = new ArrayList<>();
        ENTITY_MANAGER = repository.getEntityManager();
        ENTITY_MANAGER.getTransaction().begin();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            USER_ID_COUNTER++;
            USER = userService.create("session_rep_usr_" + USER_ID_COUNTER, "1");
            userList.add(USER);
        }
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final Session session = new Session();
            session.setUser(userList.get(i));
            session.setRole(Role.USUAL);
            repository.add(userList.get(i).getId(), session);
            sessionList.add(session);
        }
        ENTITY_MANAGER.getTransaction().commit();
        ENTITY_MANAGER.getTransaction().begin();
    }

    @After
    public void ClearAfter() {
        ENTITY_MANAGER.getTransaction().commit();
        userService.clear();
    }

    @AfterClass
    public static void closeConnection() {
        ENTITY_MANAGER.close();
    }

    @Test
    public void testAddSessionPositive() {
        @NotNull Session session = new Session();
        session.setUser(userList.get(0));
        session.setRole(Role.USUAL);
        repository.add(userList.get(0).getId(), session);
        Assert.assertEquals(2, repository.getSize(userList.get(0).getId()));
    }

    @Test
    public void testClear() {
        for (@NotNull final User user : userList) {
            Assert.assertEquals(1, repository.getSize(user.getId()));
            repository.clear(user.getId());
            Assert.assertEquals(0, repository.getSize(user.getId()));
        }
    }

    @Test
    public void testFindById() {
        for (@NotNull final User user : userList) {
            Assert.assertNull(repository.findOneById(user.getId(), UUID.randomUUID().toString()));
        }
        for (@NotNull final Session session : sessionList) {
            final Session foundSession = repository.findOneById(session.getUser().getId(), session.getId());
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final Session session : sessionList) {
            Assert.assertTrue(repository.existsById(session.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNull(repository.findOneByIndex(session.getUser().getId(), 9999));
            final Session foundSession = repository.findOneByIndex(session.getUser().getId(), 1);
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testFindAll() {
        for (@NotNull final User user : userList) {
            List<Session> sessions = repository.findAll(user.getId());
            Assert.assertNotNull(sessions);
            Assert.assertEquals(1, sessions.size());
            for (@NotNull final Session session : sessionList) {
                if (session.getUser().getId().equals(user.getId()))
                    Assert.assertNotNull(
                            sessions.stream()
                                    .filter(m -> session.getId().equals(m.getId()))
                                    .filter(m -> session.getUser().getId().equals(m.getUser().getId()))
                                    .findFirst()
                                    .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final Session session : sessionList) {
            repository.removeById(session.getUser().getId(), session.getId());
            Assert.assertNull(repository.findOneById(session.getUser().getId(), session.getId()));
            Assert.assertEquals(0, repository.getSize(session.getUser().getId()));
        }
    }

    @Test
    public void testRemove() {
        for (final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUser().getId()));
            repository.remove(session.getUser().getId(), session);
            Assert.assertEquals(0, repository.getSize(session.getUser().getId()));
        }
    }

    @Test
    public void testRemoveWOUserId() {
        for (final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUser().getId()));
            repository.remove(session);
            Assert.assertEquals(0, repository.getSize(session.getUser().getId()));
        }
    }

}
