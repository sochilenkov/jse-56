package ru.t1.sochilenkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationVersionResponse extends AbstractResponse {

    private String version;

}
