package ru.t1.sochilenkov.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sochilenkov.tm.dto.request.UserUpdateProfileRequest;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Update profile of current user.";

    @NotNull
    public static final String NAME = "user-update-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER NEW FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();

        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);

        userEndpoint.updateUserProfile(request);
    }

}
